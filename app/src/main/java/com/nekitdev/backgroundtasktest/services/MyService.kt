package com.nekitdev.backgroundtasktest.services

import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.app.Service
import android.content.Intent
import android.os.Build
import android.os.IBinder
import androidx.core.app.NotificationCompat
import com.nekitdev.backgroundtasktest.MainActivity
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch

/**
 * Created by Nikita R. on 04.10.2019.
 */
class MyService : Service() {

    private val CHANNEL_ID = "MyServiceChannel"

    override fun onBind(intent: Intent?): IBinder? {
        return null
    }

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        val input = intent?.getStringExtra("inputExtra")
        createNotificationChannel()
        val notificationIntent = Intent(this, MainActivity::class.java)
        val pendingIntent = PendingIntent.getActivity(
            this,
            0, notificationIntent, 0
        )

        val notification = NotificationCompat.Builder(this, CHANNEL_ID).apply {
            setContentTitle("Foreground Service")
            setContentText(input)
//            setSmallIcon(R.drawable.ic_delete)
            setContentIntent(pendingIntent)
        }.build()

        //stop automatically
//        stopServ(startId)

        startForeground(1, notification)
        return START_NOT_STICKY
    }

    private fun stopServ(startId: Int) {
        GlobalScope.launch {
            delay(5000)
            stopSelfResult(startId)
        }
    }

    private fun createNotificationChannel() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val serviceChannel = NotificationChannel(
                CHANNEL_ID, "Service Channel", NotificationManager.IMPORTANCE_DEFAULT
            )

            getSystemService(NotificationManager::class.java).apply {
                this!!.createNotificationChannel(serviceChannel)
            }
        }
    }
}