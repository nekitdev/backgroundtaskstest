package com.nekitdev.backgroundtasktest.services

import android.app.IntentService
import android.content.Intent
import android.util.Log

/**
 * Created by Nikita R. on 07.10.2019.
 */
class MyIntentServices(name: String = "") : IntentService(name) {

    private val TAG = MyIntentServices::class.java.simpleName

    companion object {
        const val INTENT_SERVICE_TASK = "com.nekitdev.backgroundtasktest.services.INTENT_SERVICE_TASK"
    }

    override fun onHandleIntent(intent: Intent?) {
        val extra = intent?.getStringExtra(INTENT_SERVICE_TASK)
        //simulate task processing
        if (extra.equals("FIRST_TASK")){
            Thread.sleep(1000)
        } else {
            Thread.sleep(3000)
        }
        Log.d(TAG, extra)
    }
}