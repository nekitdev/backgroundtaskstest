package com.nekitdev.backgroundtasktest.broadcastReceiver

import android.app.DownloadManager
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.util.Log

/**
 * Created by Nikita R. on 04.10.2019.
 */
class DownloadManagerCallbackReceiver : BroadcastReceiver() {

    private val TAG = DownloadManagerCallbackReceiver::class.java.simpleName

    override fun onReceive(ctx: Context?, intent: Intent?) {
        if (intent?.action.equals(DownloadManager.ACTION_DOWNLOAD_COMPLETE)) {
            Log.d(TAG, "ACTION_DOWNLOAD_COMPLETE")
        }
    }
}