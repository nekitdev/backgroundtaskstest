package com.nekitdev.backgroundtasktest.broadcastReceiver

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.util.Log

/**
 * Created by Nikita R. on 07.10.2019.
 */

class AlarmMangerCallbackReceiver : BroadcastReceiver() {

    val TAG = AlarmMangerCallbackReceiver::class.java.simpleName

    companion object {
        const val EVENT_ACTION = "com.nekitdev.backgroundtasktest.broadcastReceiver.EVENT_ACTION"
        const val ALARM_EVENT = "com.nekitdev.backgroundtasktest.broadcastReceiver.ALARM_EVENT"
    }

    override fun onReceive(context: Context?, intent: Intent?) {
        if (intent?.action.equals(EVENT_ACTION)) {
            try {
                //sending event to another broadcast, for case when we need to get event at UI
                context?.sendBroadcast(Intent(ALARM_EVENT))
            } catch (ex: Exception) {
                Log.e(TAG, ex.message, ex)
            }
        }
    }
}