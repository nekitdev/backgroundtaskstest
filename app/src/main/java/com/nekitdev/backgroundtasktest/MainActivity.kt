package com.nekitdev.backgroundtasktest

import android.app.DownloadManager
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.nekitdev.backgroundtasktest.alarmManager.startAlarmManager
import com.nekitdev.backgroundtasktest.alarmManager.stopAlarmManager
import com.nekitdev.backgroundtasktest.broadcastReceiver.AlarmMangerCallbackReceiver.Companion.ALARM_EVENT
import com.nekitdev.backgroundtasktest.broadcastReceiver.DownloadManagerCallbackReceiver
import com.nekitdev.backgroundtasktest.downloadManager.MyDownloadManager
import com.nekitdev.backgroundtasktest.services.MyIntentServices
import com.nekitdev.backgroundtasktest.services.MyIntentServices.Companion.INTENT_SERVICE_TASK
import com.nekitdev.backgroundtasktest.services.MyService
import com.nekitdev.backgroundtasktest.workManager.MyWorkManager
import kotlinx.android.synthetic.main.activity_main.*
import androidx.work.WorkManager
import androidx.lifecycle.Observer
import androidx.work.OneTimeWorkRequest
import androidx.work.PeriodicWorkRequest
import java.util.concurrent.TimeUnit


class MainActivity : AppCompatActivity() {

    private var downloadManagerCallbackReceiver: DownloadManagerCallbackReceiver? = null
    private val downloadManager by lazy { MyDownloadManager(applicationContext, "http://speedtest.ftp.otenet.gr/files/test10Mb.db") }
    private val oneTimeWorkRequest = OneTimeWorkRequest.Builder(MyWorkManager::class.java).build()
    private val periodicWorkRequest = PeriodicWorkRequest.Builder(MyWorkManager::class.java, 15, TimeUnit.MINUTES).build()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        buttonClickListener()
        observeToWorkManagerMessage()
    }

    private fun buttonClickListener() {
        btn_start_download_manager.setOnClickListener { startDownloadManager() }
        btn_stop_download_manager.setOnClickListener { stopDownloadManager() }
        btn_start_service.setOnClickListener { startService() }
        btn_stop_service.setOnClickListener { stopService() }
        btn_start_intent_service.setOnClickListener { startIntentService() }
        btn_start_alarm_manager.setOnClickListener { startAlarmManager() }
        btn_stop_alarm_manager.setOnClickListener { stopAlarmManager() }
        btn_start_work_manager.setOnClickListener { startWorkManager() }
        btn_stop_work_manager.setOnClickListener { stopWorkManager() }
    }

    private fun stopWorkManager() {
//        WorkManager.getInstance().cancelWorkById(oneTimeWorkRequest.id)
        WorkManager.getInstance().cancelWorkById(periodicWorkRequest.id)
    }

    private fun startWorkManager() {
//        WorkManager.getInstance().enqueue(oneTimeWorkRequest)
        WorkManager.getInstance().enqueue(periodicWorkRequest)
    }

    private fun observeToWorkManagerMessage() {
//        WorkManager.getInstance().getWorkInfoByIdLiveData(oneTimeWorkRequest.id).observe(this, Observer {
//            Toast.makeText(this, "once: ${it.state.name}", Toast.LENGTH_SHORT).show()
//        })

        WorkManager.getInstance().getWorkInfoByIdLiveData(periodicWorkRequest.id).observe(this, Observer {
            Toast.makeText(this, "periodic: ${it.state.name}", Toast.LENGTH_SHORT).show()
        })
    }

    private fun startDownloadManager() {
        downloadManager.downloadFile()
    }

    private fun stopDownloadManager() {
        downloadManager.stopDownloadAllFiles()
    }

    private fun startService() {
        startService(Intent(this, MyService::class.java))
    }

    private fun stopService() {
        stopService(Intent(this, MyService::class.java))
    }

    private fun startIntentService() {

        val firstIntent = Intent(this, MyIntentServices::class.java)
        firstIntent.putExtra(INTENT_SERVICE_TASK, "FIRST_TASK")
        startService(firstIntent)

        val secondIntent = Intent(this, MyIntentServices::class.java)
        secondIntent.putExtra(INTENT_SERVICE_TASK, "SECOND_TASK")
        startService(secondIntent)
    }

    private fun startAlarmManager() {
        startAlarmManager(applicationContext, 5000)
    }

    private fun stopAlarmManager() {
        stopAlarmManager(applicationContext)
    }

    override fun onStart() {
        super.onStart()
        registerBroadcast()
    }

    override fun onStop() {
        super.onStop()
        unregisterBroadcast()
    }

    private val toastBroadcastReceiver: BroadcastReceiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context, intent: Intent) {
            if (intent.action.equals(ALARM_EVENT)) {
                Toast.makeText(this@MainActivity, "ALARM", Toast.LENGTH_SHORT).show()
            }
        }
    }

    private fun registerBroadcast() {
        downloadManagerCallbackReceiver = DownloadManagerCallbackReceiver()
        registerReceiver(downloadManagerCallbackReceiver, IntentFilter(DownloadManager.ACTION_DOWNLOAD_COMPLETE))
        registerReceiver(toastBroadcastReceiver, IntentFilter(ALARM_EVENT))
    }

    private fun unregisterBroadcast() {
        unregisterReceiver(downloadManagerCallbackReceiver)
        unregisterReceiver(toastBroadcastReceiver)
    }
}
