package com.nekitdev.backgroundtasktest.workManager

import android.content.Context
import android.util.Log
import androidx.work.Worker
import androidx.work.WorkerParameters


/**
 * Created by Nikita R. on 11.10.2019.
 */
class MyWorkManager constructor(context: Context, workerParams: WorkerParameters) :
    Worker(context, workerParams) {

    val TAG = MyWorkManager::class.java.simpleName

    override fun doWork(): Result {
        for (i in 1..100) {
            Thread.sleep(100)
            Log.d(TAG, i.toString())
        }
        return Result.success()
    }
}