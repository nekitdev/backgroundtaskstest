package com.nekitdev.backgroundtasktest.alarmManager

import android.app.AlarmManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.os.SystemClock
import com.nekitdev.backgroundtasktest.broadcastReceiver.AlarmMangerCallbackReceiver
import com.nekitdev.backgroundtasktest.broadcastReceiver.AlarmMangerCallbackReceiver.Companion.EVENT_ACTION

/**
 * Created by Nikita R. on 07.10.2019.
 */

private var alarmManager: AlarmManager? = null
private lateinit var alarmIntent: PendingIntent

fun startAlarmManager(context: Context?, millisecondsToElapse: Long) {

    alarmManager = context?.getSystemService(Context.ALARM_SERVICE) as AlarmManager
    alarmIntent = Intent(context, AlarmMangerCallbackReceiver::class.java).let { intent ->
        intent.action = EVENT_ACTION
        PendingIntent.getBroadcast(context, 0, intent, 0)
    }

    alarmManager?.set(AlarmManager.ELAPSED_REALTIME_WAKEUP,
        SystemClock.elapsedRealtime() + millisecondsToElapse, alarmIntent)
}

fun stopAlarmManager(context: Context?){

    alarmManager = context?.getSystemService(Context.ALARM_SERVICE) as AlarmManager
    alarmIntent = Intent(context, AlarmMangerCallbackReceiver::class.java).let { intent ->
        intent.action = EVENT_ACTION
        PendingIntent.getBroadcast(context, 0, intent, 0)
    }

    alarmManager?.cancel(alarmIntent)
}