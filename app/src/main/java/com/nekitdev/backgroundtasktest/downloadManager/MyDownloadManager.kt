package com.nekitdev.backgroundtasktest.downloadManager

import android.app.DownloadManager
import android.content.Context
import android.net.Uri
import java.io.File

/**
 * Created by Nikita R. on 03.10.2019.
 */

class MyDownloadManager(private val ctx: Context, uri: String) {

    private val downloadManager = ctx.getSystemService(Context.DOWNLOAD_SERVICE) as DownloadManager

    private val downloadRequest = DownloadManager.Request(Uri.parse(uri)).apply {
        setTitle("My File")// Title of the Download Notification
        setDescription("Downloading")// Description of the Download Notification
        setAllowedNetworkTypes(DownloadManager.Request.NETWORK_WIFI or DownloadManager.Request.NETWORK_MOBILE)
        setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED)// Visibility of the download Notification
        setDestinationUri(Uri.fromFile(File(ctx.getExternalFilesDir(null), System.currentTimeMillis().toString())))// Uri of the destination file
        setRequiresCharging(false)// Set if charging is required to begin the download
        setAllowedOverMetered(true)// Set if download is allowed on Mobile network
        setAllowedOverRoaming(false)// Set if download is allowed on roaming network
    }

    private var downloadIDsList = arrayListOf<Long>()

    fun downloadFile() {
        val downloadID = downloadManager.enqueue(downloadRequest)
        downloadIDsList.add(downloadID)
    }

    fun stopDownloadAllFiles() {
        if (downloadIDsList.isNotEmpty()) {
            for (downloadID in downloadIDsList) {
                downloadManager.remove(downloadID)
            }
        }
        downloadIDsList.clear()
    }
}

